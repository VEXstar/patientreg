package ru.vexcore.pts.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vexcore.pts.model.entity.Patient;


import java.util.Date;
import java.util.List;

@Service
@Mapper

public interface PatientMapper {

    List<Patient> findAllBy(@Param("term") String term, @Param("birthday") Date birthday);

    List<Patient> getBy(@Param("snils") String snils);

    void add (@Param("patient") Patient patient);

    void update(@Param("snils") String snils, @Param("patient") Patient patient);

    void  delete(@Param("snils") String snils);


}
