package ru.vexcore.pts.config;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@MapperScan("ru.vexcore.pts.dao.mapper")
@PropertySource("classpath:database.properties")
public class MyBatisConfig {

    @Value("${datasource.url}")
    private String dbUrl;
    @Value("${datasource.username}")
    private String dbUser;
    @Value("${datasource.password}")
    private String dbPass;
    @Value("${datasource.driver-class-name}")
    private String dbDriver;
    @Value("${datasource.max-connections}")
    private Integer maxActiveConnections;


    @Bean
    public DataSource dataSource() {
        PooledDataSource dataSource = new PooledDataSource(dbDriver, dbUrl, dbUser, dbPass);
        dataSource.setPoolMaximumActiveConnections(maxActiveConnections);
        org.apache.ibatis.logging.LogFactory.useLog4JLogging();
        return dataSource;
    }
}
