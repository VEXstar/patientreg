package ru.vexcore.pts.model.dto;

import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import java.util.Date;

public class PatientDTO {

    @NotNull
    @Past
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthday;
    @NotNull
    @Size(min = 3, max = 3)
    private String gender;
    @NotNull
    @Size(min = 14, max = 14)
    private String snils;
    @NotNull
    @Size(min = 2, max = 16)
    private String  firstname;
    @NotNull
    @Size(min = 2, max = 16)
    private String  lastname;


    @Size(min = 2, max = 16)
    private String  middlename;

    public PatientDTO() {
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }
}
