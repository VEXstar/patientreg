package ru.vexcore.pts.model.entity;

import org.springframework.http.HttpStatus;

public class Response {
    String errorMessage;
    HttpStatus status;
    Object response;



    public Response(HttpStatus status, Object response,String errorMessage) {
        this.errorMessage = errorMessage;
        this.status = status;
        this.response = response;
    }

    public Response(HttpStatus status) {
        this.status = status;
    }

    public Response(HttpStatus status,String errorMessage) {
        this.errorMessage = errorMessage;
        this.status = status;
    }

    public Response(HttpStatus status, Object response) {
        this.status = status;
        this.response = response;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }
}
