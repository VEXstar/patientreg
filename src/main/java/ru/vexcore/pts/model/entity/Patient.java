package ru.vexcore.pts.model.entity;

import ru.vexcore.pts.model.dto.PatientDTO;

import java.util.Date;


public class Patient{

    private Date birthDay;
    private String gender;
    private String snils;

    private String  firstName;
    private String  lastName;
    private String  middleName;

    public Patient() {
    }

    public Patient(String firstName, String lastName, String middleName,Date birthDay, String gender, String snils) {
        this.birthDay = birthDay;
        this.gender = gender;
        this.snils = snils;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
    }
    public Patient(PatientDTO patientDTO){
        this.birthDay = patientDTO.getBirthday();
        this.gender = patientDTO.getGender();
        this.snils = patientDTO.getSnils();
        this.firstName = patientDTO.getFirstname();
        this.lastName = patientDTO.getLastname();
        this.middleName = patientDTO.getMiddlename();
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
