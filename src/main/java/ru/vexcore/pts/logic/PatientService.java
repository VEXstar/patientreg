package ru.vexcore.pts.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.vexcore.pts.dao.mapper.PatientMapper;
import ru.vexcore.pts.logic.util.SnilsChecker;
import ru.vexcore.pts.model.dto.PatientDTO;
import ru.vexcore.pts.model.entity.Patient;
import ru.vexcore.pts.model.entity.Response;


import java.util.Date;
import java.util.List;

@Service
public class PatientService {

   @Autowired
    PatientMapper patientMapper;


    private boolean isExists(String snils){
       return !patientMapper.getBy(snils).isEmpty();
    }

    //Добавление пациента
    public Response addPatient(PatientDTO patientDTO){

        if(!SnilsChecker.check(patientDTO.getSnils())){
            return  new Response(HttpStatus.BAD_REQUEST,"СНИЛС не прошёл проверку");

        }

        if(isExists(patientDTO.getSnils())){
            return  new Response(HttpStatus.BAD_REQUEST,"Указанный СНИЛС уже используется");
        }
        Patient patient = new Patient(patientDTO);
        patientMapper.add(patient);
        return  new Response(HttpStatus.CREATED);
    }

    public Response updatePatient(String snils,PatientDTO patientDTO){
        if(!isExists(snils)){
            return  new Response(HttpStatus.NOT_FOUND,"Пациент с указанным СНИЛС не найден");
        }
        if(patientDTO.getSnils()!=null&&!patientDTO.getSnils().equals(snils)){
            if(!SnilsChecker.check(patientDTO.getSnils())){
                return  new Response(HttpStatus.BAD_REQUEST,"Новый СНИЛС не прошёл проверку");
            }
            if(isExists(patientDTO.getSnils())){
                return  new Response(HttpStatus.BAD_REQUEST,"Указанный новый СНИЛС уже используется");
            }
        }

        Patient patient = new Patient(patientDTO);
        patientMapper.update(snils,patient);

        return  new Response(HttpStatus.OK);
    }
    public Response deletePatientBy(String snils){
        if(!isExists(snils)){
            return  new Response(HttpStatus.NOT_FOUND,"Пациент с указанным СНИЛС не найден");
        }
        patientMapper.delete(snils);
        return  new Response(HttpStatus.OK);
    }

    public Response getPatientBy(String snils){
        List<Patient> patient = patientMapper.getBy(snils);
        if(patient.isEmpty()){
            return  new Response(HttpStatus.NOT_FOUND,"Пациент с указанным СНИЛС не найден");
        }
        else
            return  new Response(HttpStatus.OK,patient.get(0));
    }
    public Response findPatientBy(String term ,Date birthDay){
        List<Patient> patients = patientMapper.findAllBy(term,birthDay);
        if(patients.isEmpty())
            return  new Response(HttpStatus.NOT_FOUND,"Поиск не дал результатов");
        return  new Response(HttpStatus.OK,patients);
    }


}
