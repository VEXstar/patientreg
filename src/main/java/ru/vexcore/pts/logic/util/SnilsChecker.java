package ru.vexcore.pts.logic.util;


import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SnilsChecker {

    public static boolean check(Object elem) {
        if(!(elem instanceof String))
            return false;
        String snils = (String) elem;

        return templateCheck(snils)&&hashCheck(snils);
    }
    private static boolean templateCheck(String snils) {
        Pattern pattern = Pattern.compile("^(\\d{3})-(\\d{3})-(\\d{3}) (\\d{2})$");
        Matcher matcher = pattern.matcher(snils);
        return matcher.find();
    }
    private static boolean hashCheck(String snils){
        String[] sep = snils.replaceAll("-","").replaceAll(" ","").split("");
        int[] nums = Arrays.stream(sep).mapToInt(Integer::parseInt).toArray();
        int calcSum = 0;
        int mul = 9;
        for(int i = 0;i<nums.length-2;i++){
            calcSum += (mul--)*nums[i];
        }
        int sum = nums[9]*10 + nums[10];
        calcSum = (calcSum==100)?0:calcSum;
        return sum==calcSum%101;
    }
}
