package ru.vexcore.pts.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import ru.vexcore.pts.logic.PatientService;
import ru.vexcore.pts.model.dto.PatientDTO;
import ru.vexcore.pts.model.entity.Response;


import javax.validation.Valid;
import java.util.Date;
import java.util.stream.Collectors;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/api/patient", produces = "text/plain;charset=UTF-8")
public class PatientApiController {

    @Autowired
    PatientService patientService;

    private Gson gson = new GsonBuilder().create();


    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<String> addPatient(@Valid PatientDTO patientDTO, BindingResult result)  {
        if(!result.hasErrors())
            return responseGen(patientService.addPatient(patientDTO));
        return responseGen(result);

    }

    //Update
    @RequestMapping(value = "/{snils}", method = RequestMethod.PUT)
    ResponseEntity<String> updatePatientBySnils(@PathVariable String snils, @Valid PatientDTO patientDTO, BindingResult result){
        if(!result.hasErrors())
            return responseGen(patientService.updatePatient(snils,patientDTO));
        return responseGen(result);
    }

    //Read
    @RequestMapping(value = "/{snils}", method = RequestMethod.GET)
    ResponseEntity<String> getPatientBySnils(@PathVariable String snils){
        return responseGen(patientService.getPatientBy(snils));
    }
    @RequestMapping(value = "/", method = RequestMethod.GET)
    ResponseEntity<String> findAllPatient(String term, Date date){
        return responseGen(patientService.findPatientBy(term,date));
    }

    //Delete
    @RequestMapping(value = "/{snils}", method = RequestMethod.DELETE)
    ResponseEntity<String> deletePatientBySnils(@PathVariable String snils){
        return responseGen(patientService.deletePatientBy(snils));
    }

    private ResponseEntity<String> responseGen(Response response){
        return ResponseEntity.status(response.getStatus()).body(gson.toJson(response));
    }
    private ResponseEntity<String> responseGen(BindingResult result){
        StringBuilder builder = new StringBuilder("Обнаружены пустые/недопустимые поля: ");
        builder.append(result.getFieldErrors().stream().map(FieldError::getField).collect(Collectors.joining(", ")));
        Response response = new Response(HttpStatus.BAD_REQUEST,builder.toString());
        String json = gson.toJson(response);
        return ResponseEntity.badRequest().body(json);
    }




}
